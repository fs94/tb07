package AB01;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * Flugzeugverwaltung_Datei
 * 
 * 
 * 
 * 
 * 
 * Dev-Info
 * 	get kNR
 * 	data.split(": ")[1]			
 * 		
 * 	get Time
 * 	data.split(" - ", 2)[0]
 * 	data.split(" ")[2]
 * 
 *  Arraymapping
 *  0 1  2  3  4  5  6  7  8  9 10 11 12 -- Array Pos
 *  8 9 10 11 12 13 14 15 16 17 18 19 20 -- Zeit
 * 
 * 
 * @author matthias.amelung
 * @version 1.0
 * FS94
 *
 */

// P Anfang
public class Flugzeugverwaltung_Datei {

	final static String FILENAME = "c:/temp/buchung.txt";	// Dateiort und Name
	
	// HP Anfang
	public static void main(String[] args) {
		
		// D Anfang
		Scanner sc = new Scanner(System.in);	// Scanner fuer Eingabe
		
		// ArrayList<String> buchung = new ArrayList<String>();
		int option = 0;	// option fuer Switch-Menu
		int zeitStart;	// Buchungsstartzeit
		int zeitEnde;	// Buchungsendzeit
		int kNR;	// Kundennummer
		boolean wdh = true;	// wdh vom Programm
		// D Ende
		
		buchungFileCreate();
		
		do {
					
			option = menu(sc, option); // Ausgabe Menu und Eingabe switch-case Option
			
			switch (option) {
			case 1:
				// Eingabe der Buchung
				ausgabe("\n --- Buchung eingeben --- \n");
				
				// Ueberpruefung ob Startzeit auch vor der Endzeit liegt 
				do {
					do {
						zeitStart = eingabe(sc, "Geben Sie die Startzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitStart
						zeitEnde = eingabe(sc, "Geben Sie die Endzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitEnde
					} while (zeitEnde < 8 || zeitEnde > 20);  // zeit nur zwischen 8 und 20 Uhr
				} while (zeitStart > zeitEnde); // startzeit darf nicht h�her als Endzeit sein
				
				// Eingabe Kundennummer
				kNR = eingabe(sc, "Geben Sie die Kundennummer ein: ");
				
				buchungEintragen(zeitStart, zeitEnde, kNR);
				break;
			case 2:
				// Ausgabe aller Buchungen, auch freier Slots
				ausgabe("\n --- Buchung anzeigen --- \n");
				ausgabe(buchungFileGetData());
				break;
			case 3:
				// Loeschen einer Buchung ueber Startzeit, Endzeit und Kundennummer
				ausgabe("\n --- Buchung loeschen --- \n");
				do {
					do {
						zeitStart = eingabe(sc, "Geben Sie die Startzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitStart 
						zeitEnde = eingabe(sc, "Geben Sie die Endzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitEnde
					} while (zeitEnde < 8 || zeitEnde > 20); // zeit nur zwischen 8 und 20 Uhr
				} while (zeitStart > zeitEnde); // startzeit darf nicht h�her als Endzeit sein
				
				kNR = eingabe(sc, "Geben Sie die Kundennummer der Buchung ein: ");
				
				buchungFileDeleteDataFromLine(buchungFileGetLine(zeitStart, zeitEnde, kNR));
				
				// Aufruf buchungLoeschen
//				buchungLoeschen(buchung, zeitStart, zeitEnde, kNR);
				
				break;
			case 4:
				// wdh auf false setzen und Programm beenden
				ausgabe("\nAuf Wiedersehen!");
				wdh = false;
				break;
			default:
				break;
			}
			
		} while (wdh);
		
		// Scanner schlie�en
		sc.close();
		
	}
	// HP Ende

	
	/*
	 * 
	 * Methoden
	 * 
	 */
	
	// Eingabe-Methode
	// bekommt Scanner und Ausgabetext
	// gibt Integer-Eingabe zurueck
	private static int eingabe(Scanner sc, String text) {
		System.out.print(text);
		return sc.nextInt();
	};
	
	// Ausgabe-Methode
	// gibt uebergebenen String aus
	private static void ausgabe(String text) {
		System.out.println(text);
	}
	
	private static void fileNotFound() {
		System.out.println("\nBuchung konnte nicht eingetragen werden. Ueberpruefen Sie die Datei.");
	}
	
	// Wandelt Integer in String um
	private static String intToStr(int n) {
		return String.valueOf(n);
	}
	
	// Wandelt String in Integer um
	private static int strToInt(String s) {
		return Integer.parseInt(s);
	}
	
	private static void buchungEintragen(int zeitStart, int zeitEnde, int kNR) {
		String data = null;
		if (buchungCheck(zeitStart, zeitEnde, kNR)) {
			data = intToStr(zeitStart) + " - " + intToStr(zeitEnde) + " fuer die Kundennummer: " + intToStr(kNR);
			buchungFileWriteData(data);
			
		} else {
			ausgabe("Der Slot von " + zeitStart + " bis " + zeitEnde + " Uhr ist belegt.\nDies sind noch freie Slots:");
			buchungFrei();
		}		
	}
	
	// ueberprueft ob zeitslot noch frei ist
	private static boolean buchungCheck(int zeitStart, int zeitEnde, int kNR) {
		int lineNrs = buchungFileGetLineCount();
		String[] buchung = new String[lineNrs];
		boolean free = false;
		for (int i = 0; i < buchung.length; i++) {
			buchung[i] = buchungFileGetDataFromLine(lineNrs, i);
			if (!(strToInt(buchung[i].split(" - ", 2)[0]) <= zeitStart && strToInt(buchung[i].split(" ")[2]) >= zeitStart)) {
				free = true;
			}
		}
		return free;
	}
	
	// zeigt freie zeitslots
	private static void buchungFrei() {
		ArrayList<String> buchungFile = new ArrayList<String>();
		int[] buchung = new int[13];
		int zeitStart;
		int zeitEnde;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(FILENAME));
			while (br.ready()) {
				buchungFile.add(br.readLine());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (int i = 0; i < buchungFile.size(); i++) {
			zeitStart = strToInt(buchungFile.get(i).split(" - ", 2)[0]);
			zeitEnde = strToInt(buchungFile.get(i).split(" ")[2]);
			for (int j = (zeitStart-8); j < (zeitEnde-7); j++) {
				buchung[j] = 1;
			}
		}
		
		for (int i = 0; i < buchung.length; i++) {
			if (buchung[i] == 0) {
				ausgabe((i+8) + " Uhr ist noch frei."); 
			}
		}
	}

	// Prueft und/oder erstellt eine Buchungsdatei
		private static void buchungFileCreate() {
			try {
				File file = new File(FILENAME);
				if (file.createNewFile()) {
					ausgabe("Neue Buchungsdatenbank erstellt.");
					ausgabe("Pfad: " + FILENAME);
				} else {
					ausgabe("Vorhandene Datenbank wird genutzt");
					ausgabe("Pfad: " + FILENAME);
				}
			} catch (IOException e) {
				ausgabe("\nDatenbank konnte nicht angelegt werden. Ueberpruefen Sie den Pfad.");
				e.printStackTrace();
			}
		}
	
	// schreibt Daten in die Buchungsdatei
	private static void buchungFileWriteData(String data) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(FILENAME, true));
			// sort data
			writer.write(data);
			writer.newLine();
			writer.close();
			ausgabe("\nBuchung erfolgreich eingetragen");
		} catch (IOException e) {
			fileNotFound();
			e.printStackTrace();
		}
	}
	
	// gibt komplette Buchungsdatei aus
	private static String buchungFileGetData() {
		String data = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(FILENAME));
			for (int i = 0; i < buchungFileGetLineCount(); i++) {
				data = data + br.readLine() + "\n";
			}
			br.close();
		} catch (IOException e) {
			fileNotFound();
			e.printStackTrace();
		}
		return data;
	}
	
	// gibt bestimmte Zeile in Buchungsdatei aus
	// Optimierungspotential
	private static String buchungFileGetDataFromLine(int maxLines, int lineNr) {
		try {
			String line = null;
			BufferedReader br = new BufferedReader(new FileReader(FILENAME));
			for (int i = 0; i < maxLines; i++) {
				if (i == lineNr) {
					line = br.readLine();
					break;
				}
			}
			br.close();
			return line;
		} catch (IOException e) {
			fileNotFound();
			e.printStackTrace();
		}
		return null;
	}
	
	// gibt Anzahl der Zeilen in Buchungsdatei zurueck
	private static int buchungFileGetLineCount() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(FILENAME));
			int lines = 0;
			while (br.readLine() != null) {
				lines++;
			}
			br.close();
			return lines;
		} catch (IOException e) {
			fileNotFound();
			e.printStackTrace();
		}
		return 0;
	}
	
	// gibt Zeile mit start+ endzeit und Kundennummer zurueck
	// mapping ueber ArrayList
	// Position in ArrayList = Zeile in Textdatei (index 0)
	private static int buchungFileGetLine(int zeitStart, int zeitEnde, int kNR) {
		int lineNr = -1;
		int lineCount = buchungFileGetLineCount();
		ArrayList<String> buchung = new ArrayList<String>();
		
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(FILENAME));
			for (int i = 0; i < lineCount; i++) {
				buchung.add(br.readLine());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (int i = 0; i < buchung.size(); i++) {
			if (buchung.get(i).contains(intToStr(zeitStart)) && 
					buchung.get(i).contains(intToStr(zeitEnde)) && 
					buchung.get(i).contains(intToStr(kNR))) {
				lineNr = i;
			}
		}
		
		return lineNr;
	}
	
	// loescht Zeile in Buchung-Datei
	private static void buchungFileDeleteDataFromLine(int lineNr) {
		if (lineNr < 0) {
			ausgabe("\nDie Buchung wurde nicht gefunden.\n");
		} else {
			// find line
			// delete line
			// sort data in file
		}
	}
	
	
	// Menu-Klasse
	// generiert Menue und erwartet Eingabe
	private static int menu(Scanner sc, int option) {
		do {
			ausgabe("\n+---------------------------------------+");
			ausgabe("|                                       |");
			ausgabe("|       Flugzeugbuchungsverwaltung      |");
			ausgabe("|                                       |");
			ausgabe("|        1  Buchung eingeben            |");
			ausgabe("|        2  Buchung anzeigen            |");
			ausgabe("|        3  Buchung loeschen            |");
			ausgabe("|        4  Programm beenden            |");
			ausgabe("|                                       |");
			ausgabe("+---------------------------------------+\n");
			option = eingabe(sc, "Waehlen Sie eine Option zwischen 1 und 4: ");
		} while (option < 1 || option > 4);
		return option;
	}
	
	
}
// P Ende