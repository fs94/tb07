import java.util.Scanner;

/**
 * 
 * Flugzeugverwaltung_Dateilos
 * Flugbuchungsverwaltung mit einem Array. Buchungen haben eine Startzeit und eine Endzeit.
 * Gebucht wird ueber eine Kundennummer. Es gibt Methoden zur Ueberpruefung der freien Zeitslots,
 * belegten Slots, zum Eintragen, Anzeigen und Loeschen von Buchungen.
 * Die Zeitslots sind entsprechend dem Schemata auf das Array gemappt.
 * Eine Buchung von 8 bis 9 belegt nur den Slot 8 (0).
 * Eine Buchung von 8 bis 10 belegt die Slots 8 und 9 (0, 1).
 * Die Endzeit ist damit nicht einschliesslich inbegriffen. 
 * 
 *  Arraymapping
 *  0 1  2  3  4  5  6  7  8  9 10 11 12 -- Array Pos
 *  8 9 10 11 12 13 14 15 16 17 18 19 20 -- Zeit
 * 
 * @author matthias.amelung
 * @version 1.0
 * FS94
 *
 */

// P Beginn
public class Flugzeugverwaltung_Dateilos {

	// HP Beginn
	public static void main(String[] args) {
		
		// D Anfang
		Scanner sc = new Scanner(System.in);	// Scanner fuer Eingabe
		int[] buchung = new int[13];	// Buchungsarray
		int option = 0;		// option fuer Switch-Menu
		int zeitStart;	    // Buchungsstartzeit
		int zeitEnde;		// Buchungsendzeit
		int kNR;		// Kundennummer
		boolean wdh = true; 	// wdh vom Programm
		// D Ende
		
		do {
			option = menu(sc, option); // Ausgabe Menu und Eingabe switch-case Option
			
			switch (option) {
			case 1:
				// Eingabe der Buchung
				ausgabe("\n --- Buchung eingeben --- \n");
				
				// Ueberpruefung ob Startzeit auch vor der Endzeit liegt 
				do {
					do {
						zeitStart = eingabe(sc, "Geben Sie die Startzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitStart
						zeitEnde = eingabe(sc, "Geben Sie die Endzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitEnde
					} while (zeitEnde < 8 || zeitEnde > 20);  // zeit nur zwischen 8 und 20 Uhr
				} while (zeitStart > zeitEnde); // startzeit darf nicht höher als Endzeit sein
				
				// Eingabe Kundennummer
				kNR = eingabe(sc, "Geben Sie die Kundennummer ein: ");
				
				// Buchung wird nur eingetragen, wenn buchungCheck true zurueckgibt
				if (buchungCheck(buchung, zeitStart, zeitEnde)) {
					buchungEintragen(buchung, zeitStart, zeitEnde, kNR); // Buchung wird eingetragen
				} else {
					// Ausgabe, dass Buchungsslot belegt ist, sowie noch freie Buchungsslots
					System.out.println("\nDer Zeitslot von "+ zeitStart + " bis " + zeitEnde + " Uhr ist leider schon belegt.");
					System.out.println("Hier sind freie Zeitslots: ");
					buchungFrei(buchung);
				}
				break;
			case 2:
				// Ausgabe aller Buchungen, auch freier Slots
				ausgabe("\n --- Buchung anzeigen --- \n");
				buchungAnzeigen(buchung);
				break;
			case 3:
				// Loeschen einer Buchung ueber Startzeit, Endzeit und Kundennummer
				ausgabe("\n --- Buchung loeschen --- \n");
				do {
					do {
						zeitStart = eingabe(sc, "Geben Sie die Startzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitStart 
						zeitEnde = eingabe(sc, "Geben Sie die Endzeit der Buchung ein (8 - 20): "); // Aufruf, Eingabe von zeitEnde
					} while (zeitEnde < 8 || zeitEnde > 20); // zeit nur zwischen 8 und 20 Uhr
				} while (zeitStart > zeitEnde); // startzeit darf nicht höher als Endzeit sein
				
				kNR = eingabe(sc, "Geben Sie die Kundennummer der Buchung ein: ");
				
				// Aufruf buchungLoeschen
				buchungLoeschen(buchung, zeitStart, zeitEnde, kNR);
				
				break;
			case 4:
				// wdh auf false setzen und Programm beenden
				System.out.println("\nAuf Wiedersehen!");
				wdh = false;
				break;
			default:
				break;
			}
			
		} while (wdh);
		
		// Scanner schließen
		sc.close();
	}
	// HP Ende
	
	// Eingabe-Methode
	// bekommt Scanner-Object und Ausgabestring
	// gibt Integer zurueck
	private static int eingabe(Scanner sc, String string) {
		System.out.print(string);
		return sc.nextInt();
	}
	
	// Ausgabe-Methode
	// bekommt Ausgabestring
	private static void ausgabe(String string) {
		System.out.println(string);
	}
	
	// buchungEintragen
	// bekommt array mit Buchungen, start + endzeit und Kundennummer
	// Ueberprueft ob nur 1h-Zeitslot und traegt Buchung mit Kundennummer ein
	private static void buchungEintragen(int[] buchung, int zeitStart, int zeitEnde, int kNR) {
		if ((zeitEnde-zeitStart) == 1) {
			buchung[zeitStart-8] = kNR;
		} else {
			for (int i = (zeitStart-8); i < (zeitEnde-8); i++) {
				buchung[i] = kNR;
			}
		}
		System.out.println("\nBuchung erfolgreich eingetragen.");
	}

	// buchungAnzeigen
	// bekommt buchungs-array
	// looped durch buchung[] durch und gibt an, ob gebucht oder nicht
	private static void buchungAnzeigen(int[] buchung) {
		for (int i = 0; i < buchung.length; i++) {
			if (buchung[i] != 0) {
				System.out.println((i + 8) + " Uhr gebucht fuer Kundennummer " + buchung[i]);
			} else {
				System.out.println((i+8) + " Uhr ist noch nicht gebucht.");
			}
		}
	}

	// buchungLoeschen
	// bekommt array buchung, start + endzeit und Kundennummer
	// geht durch Start und Endzeit durch und loescht die Kundennummer -- kann auch den gesamten Buchungstag loeschen
	// verhindert, dass ein ganzer Buchungstag geloescht werden kann
	private static void buchungLoeschen(int[] buchung, int zeitStart, int zeitEnde, int kNR) {
		int zeitTmp = 0; // temporaere Ausgabe-Variable
		for (int i = (zeitStart-8); i < (zeitEnde-8); i++) {
			if (buchung[i] == kNR) { // Nur Buchung der kNR kann geloescht werden
				buchung[i] = 0;
				if (zeitTmp == 0) { // Erster Zeitslot in buchung
					zeitTmp = i+8;
				}
			} else { // sobald ein anderer Buchungsslot erkannt wird, bricht die Schleife ab
				ausgabe("\nAnderen Buchungsslot erreicht.");
				ausgabe("Buchungen der Kundennummer " + kNR + "\n von " + zeitTmp + " bis " + (i+8) + " Uhr wurden entfernt.");
				break;
			}
		}
		ausgabe("\nBuchung erfolgreich gelöscht.");
	}
	
	// buchungCheck
	// bekommt array buchung, start + endzeit
	// ueberprueft ob array zum zeitslot noch frei ist
	private static boolean buchungCheck(int[] buchung, int zeitStart, int zeitEnde) {
		boolean free = true;
		if ((zeitEnde-zeitStart) == 1) {
			if (buchung[zeitStart-8] != 0) {
				free = false;
			}
		} else {
			for (int i = (zeitStart-8); i < (zeitEnde-8); i++) {
				if (buchung[i] != 0) {
					free = false;
				}
			}
		}
		return free;
	}
	
	// buchungFrei
	// bekommt array buchung
	// zeigt leere/offene Zeitslots
	private static void buchungFrei(int[] buchung) {
		for (int i = 0; i < buchung.length; i++) {
			if (buchung[i] == 0) {
				System.out.println((i+8) + " Uhr ist noch nicht gebucht.");
			}
		}
	}
	
	// Menu-Klasse
	// generiert Menue und erwartet Eingabe
	private static int menu(Scanner sc, int eingabe) {
		do {
			System.out.println("\n+---------------------------------------+");
			System.out.println("|                                       |");
			System.out.println("|       Flugzeugbuchungsverwaltung      |");
			System.out.println("|                                       |");
			System.out.println("|        1  Buchung eingeben            |");
			System.out.println("|        2  Buchung anzeigen            |");
			System.out.println("|        3  Buchung löschen             |");
			System.out.println("|        4  Programm beenden            |");
			System.out.println("|                                       |");
			System.out.println("+---------------------------------------+\n");
			System.out.print("Wählen Sie eine Option zwischen 1 und 4: ");
			eingabe = sc.nextInt();
		} while (eingabe < 1 || eingabe > 4);
		return eingabe;
	}
	
	
}
// P Ende